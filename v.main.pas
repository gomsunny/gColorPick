unit v.main;

interface

uses
  m.magCanvas,

  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls, System.Actions, Vcl.ActnList,
  Vcl.PlatformDefaultStyleActnCtrls, Vcl.ActnMan, Vcl.Menus;

type
  TvMain = class(TForm)
    ListColors: TListBox;
    LabelDesc: TLabel;
    PanelMag: TPanel;
    ActionManager: TActionManager;
    Timer: TTimer;
    ActionMag1: TAction;
    ActionMag2: TAction;
    ActionMag3: TAction;
    Action1: TAction;
    Action2: TAction;
    Action3: TAction;
    Action4: TAction;
    ActionCopy: TAction;
    ActionSelectAll: TAction;
    ActionClear: TAction;
    ActionDelete: TAction;
    ActionHelp: TAction;
    ActionCaptureColor: TAction;
    procedure FormCreate(Sender: TObject);

    procedure TimerTimer(Sender: TObject);
    procedure ActionMagX(Sender: TObject);
    procedure ActionMouseMove(Sender: TObject);
    procedure ActionCaptureColorExecute(Sender: TObject);
    procedure ActionCopyExecute(Sender: TObject);
    procedure ActionSelectAllExecute(Sender: TObject);
    procedure ActionClearExecute(Sender: TObject);
    procedure ActionHelpExecute(Sender: TObject);
    procedure ActionDeleteExecute(Sender: TObject);
  private
    FCanvas: TMagnifyCanvas;
    FMagX: Integer;
  public
  end;

var
  vMain: TvMain;

implementation

{$R *.dfm}

uses
  System.UITypes, Vcl.ClipBrd
  ;

procedure TvMain.ActionCaptureColorExecute(Sender: TObject);
begin
  if not FCanvas.Empty then
    ListColors.Items.Add( '#' + Integer(FCanvas.clCenter).ToHexString(8) );
end;

procedure TvMain.ActionClearExecute(Sender: TObject);
const
  SMsg = 'Are you sure you want to permanently delete these all items?';
begin
  if ListColors.Count > 0 then
    if MessageDlg(SMsg, mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      ListColors.Clear;
end;

procedure TvMain.ActionCopyExecute(Sender: TObject);
var
  LBuf: TStringList;
  i: Integer;
begin
  LBuf := TStringList.Create;
  try
    for i := 0 to ListColors.Count -1 do
      if ListColors.Selected[i] then
        LBuf.Add(ListColors.Items[i]);
    if LBuf.Count > 0 then
      Clipboard.AsText := LBuf.Text;
  finally
    FreeAndNil(LBuf);
  end;
end;

procedure TvMain.ActionDeleteExecute(Sender: TObject);
const
  SMsg = 'Are you sure you want to permanently delete these %d items?';
var
  LCnt, i: Integer;
begin
  LCnt := 0;
  for i := ListColors.Count -1 downto 0 do
    if ListColors.Selected[i] then
      Inc(LCnt);

  if LCnt = 0 then
    Exit;

  if MessageDlg(Format(SMsg, [LCnt]), mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    for i := ListColors.Count -1 downto 0 do
      if ListColors.Selected[i] then
        ListColors.Items.Delete(i);
end;

procedure TvMain.ActionHelpExecute(Sender: TObject);
begin
  MessageDlg(
    'Ctrl+`: Copy Color'#13#10+
    'Ctrl+1/2/3: x2, x4, x8'#13#10+
    'Ctrl+Left/Right/Up/Down: move mouse coursor'#13#10#13#10+
    'Ctrl+A: Select All'+#13#10+'Ctrl+C: Copy'+#13#10'Del: Delete Items'+#13#10+'Esc: Clear All'#13#10#13#10+
    'souce: github.com/gomsun2/gclpick'#13#10+
    'by gomsun2@gmail.com',

    mtInformation, [mbOK], 0);
end;

procedure TvMain.ActionMagX(Sender: TObject);
var
  LAction: TAction absolute Sender;
begin
  FMagX := LAction.Tag;
  LabelDesc.Caption := Format('Ctrl+`: Pick, F1: Help', [LAction.Caption]);
  FCanvas.Mag := LAction.Tag;
end;

procedure TvMain.ActionMouseMove(Sender: TObject);
const
  L = 0; R = 1; U = 2; D = 3;
var
  LAction: TAction absolute Sender;
  LPos: TPoint;
begin
  if BoundsRect.Contains(LPos) then
     Exit;

  GetCursorPos(LPos);
  case LAction.Tag of
    L: LPos.Offset(-1, 0);
    R: LPos.Offset(+1, 0);
    U: LPos.Offset(0, -1);
    D: LPos.Offset(0, +1);
  end;
  SetCursorPos(LPos.X, LPos.Y);
end;

procedure TvMain.ActionSelectAllExecute(Sender: TObject);
begin
  ListColors.SelectAll;
end;

procedure TvMain.FormCreate(Sender: TObject);
begin
  Screen.Cursor := crDefault;

  FCanvas := TMagnifyCanvas.Create(Self);
  FCanvas.Parent := PanelMag;
  FCanvas.Align :=alClient;

  ActionCaptureColor.ShortCut := ShortCut(VK_OEM_3, [ssCtrl]);
  ActionMag1.Execute;
end;

procedure TvMain.TimerTimer(Sender: TObject);
begin
  Timer.Enabled := False;
  try
    if WindowState <> wsMinimized then
      FCanvas.CaptureCursor;
  finally
    Timer.Enabled := True;
  end;
end;

end.
